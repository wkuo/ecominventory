DROP TABLE IF EXISTS inventory ;
CREATE TABLE inventory (
    id bigint PRIMARY KEY,
    name varchar(50),
    quantity int,
    unit varchar(3),
    price double,
    ct_dt timestamp,
    lm_dt timestamp
) ;