package com.ecom.inventory.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

@Entity
@Table(name = "inventory")
public class Inventory {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String name;
	
	private Integer quantity; 
	
	private String unit;
	
	private Double price;
	
	@CreatedDate
	@Column(name = "ct_dt")
	private Date ctDt;
	
	@LastModifiedDate
	@Column(name = "lm_dt")
	private Date lmDt;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Date getCtDt() {
		return ctDt;
	}

	public void setCtDt(Date ctDt) {
		this.ctDt = ctDt;
	}

	public Date getLmDt() {
		return lmDt;
	}

	public void setLmDt(Date lmDt) {
		this.lmDt = lmDt;
	}
}
