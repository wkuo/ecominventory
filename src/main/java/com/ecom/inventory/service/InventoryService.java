package com.ecom.inventory.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ecom.inventory.model.Inventory;
import com.ecom.inventory.model.InventoryWS;
import com.ecom.inventory.repository.InventoryRepository;


@Service
public class InventoryService implements IInventoryService {
	
	@Autowired
	InventoryRepository repo;
	
	private static final Logger log = LoggerFactory.getLogger(InventoryService.class);

	@Override
	public List<?> findAll() {
		return (List<?>) repo.findAll();

	}
	
	@Override
	public Inventory find(Long id) {
		return repo.findById(id).orElse(null);
	}
	
	@Override
	public void deductInventory(InventoryWS inv) {
		
		Long id = inv.getId();
		log.info("Service : Inventory " + id + " deducting " + inv.getQuantity());
		Inventory i = repo.findById(id).orElse(null);
		if (null != i) {
			Integer current = i.getQuantity();
			Integer newBal = current - inv.getQuantity();
			if ( 0 > newBal) {
				// Insufficient fund, stop transaction and abort
				throw new IllegalStateException("Insufficient stock");
			} else {
				i.setQuantity(newBal);
				repo.save(i);
			}
		} else {
			throw new IllegalStateException("Item not found");
		}
	}
	
	@Override
	public void addInventory(InventoryWS inv) {
		Long id = inv.getId();
		log.info("Service : Inventory " + id + " adding " + inv.getQuantity());
		Inventory i = repo.findById(id).orElse(null);
		if (null != i) {
			Integer current = i.getQuantity();
			i.setQuantity(current + inv.getQuantity());
			repo.save(i);
		} else {
			throw new IllegalStateException("Item not found");
		}
	}

}
