package com.ecom.inventory.service;

import java.util.List;

import com.ecom.inventory.model.Inventory;
import com.ecom.inventory.model.InventoryWS;

public interface IInventoryService {

	void deductInventory(InventoryWS i);
	
	void addInventory(InventoryWS i);

	List<?> findAll();

	Inventory find(Long id);

}
