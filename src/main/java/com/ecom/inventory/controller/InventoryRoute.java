package com.ecom.inventory.controller;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.stereotype.Component;

import com.ecom.inventory.model.InventoryWS;

@Component
public class InventoryRoute extends RouteBuilder{

	@Override
	public void configure() throws Exception {

		rest().get("/inventory")
			.route()
			.bean("inventoryService", "findAll")
			.marshal().json(JsonLibrary.Jackson);


		rest().put("/inventory/deduct/{id}")
			.route()
			.unmarshal().json(JsonLibrary.Jackson, InventoryWS.class)
			//.to("bean-validator:validatePayment")
			.bean("inventoryService", "deductInventory")
			.marshal().json(JsonLibrary.Jackson);
		
		rest().put("/inventory/add/{id}")
			.route()
			.unmarshal().json(JsonLibrary.Jackson, InventoryWS.class)
			//.to("bean-validator:validatePayment")
			.bean("inventoryService", "addInventory")
			.marshal().json(JsonLibrary.Jackson);
		
		}
	
}
