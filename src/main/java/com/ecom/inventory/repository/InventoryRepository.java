package com.ecom.inventory.repository;
import org.springframework.data.repository.CrudRepository;

import com.ecom.inventory.model.Inventory;

public interface InventoryRepository extends CrudRepository<Inventory, Long>{


}
